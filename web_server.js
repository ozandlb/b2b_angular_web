var fs = require('fs');
var http = require('http');
var https = require('https');
var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');

var certificate, credentials, file, files, ca;

if (process.env.NODE_ENV === 'production') {

	files = ['COMODORSAAddTrustCA.crt', 'COMODORSADomainValidationSecureServerCA.crt', 'AddTrustExternalCARoot.crt'];

	certificate = fs.readFileSync('sslcert/jamb2b_com.crt', 'utf8');

	ca = (function() {
		var _i, _len, _results;

		_results = [];
		for (_i = 0, _len = files.length; _i < _len; _i++) {
			file = files[_i];
			_results.push(fs.readFileSync("sslcert/" + file));
		}
		return _results;
	})();

	credentials = {ca: ca, key: privateKey, cert: certificate};

}


else {
	certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
	credentials = {key: privateKey, cert: certificate};
}



var express = require('express'); //,
// b2b = require('./routes/b2b');

var app = express();

var allowCrossDomain = function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	next();
};


app.configure(function () {
	app.use(express.logger('dev')); //     'default', 'short', 'tiny', 'dev'
	app.use(express.bodyParser());
	app.use(allowCrossDomain);
});

function requireHTTPS(req, res, next) {

	if (!req.secure) {

		//app.settings.env
		if (process.env.NODE_ENV === 'dev') {
			var reqPort = ':8443';
		}

		else if (process.env.NODE_ENV === 'production') {
			var reqPort = ':443';
		}

		var myurl = 'https://' + req.host + reqPort + req.url;
		console.log(myurl);

		return res.redirect(myurl);
	}
	next();
}

app.use(requireHTTPS);

app.use("/styles", express.static(__dirname + '/dist/styles'));

app.use("/images", express.static(__dirname + '/dist/images'));

app.use("/scripts", express.static(__dirname + '/dist/scripts'));

app.use("/bower_components", express.static(__dirname + '/dist/bower_components'));

app.use("/views", express.static(__dirname + '/dist/views'));

app.use("/", express.static(__dirname + '/dist/'));

//app.get('/', function(req, res) {
//  res.sendfile('dist/index.html')
//});

// app.get('*', function(req, res) {
//   res.sendfile('./app/index.html');
// });

// app.get('/survey', function(req, res) {
//   res.redirect('https://docs.google.com/forms/d/1OB9YkNpGxokYQoN04hbvqUIZuiFT-EDU3UKpKt_DIMM/viewform')
// });

// app.post('/login', b2b.userLogin);
// app.get('/users', b2b.usersFindAll);
// app.get('/users/:id', b2b.usersFindById);
// app.post('/users', b2b.userAdd);
// app.put('/users/:id', b2b.usersUpdate);
// app.get('/companies', b2b.companiesFindAll);
// app.get('/companies/:id', b2b.companyFindById);
// app.post('/companies', b2b.companiesAdd);
// app.put('/companies/:id', b2b.companyUpdate);
// app.get('/companies/:id/products', b2b.productFindByCompanyId);
// app.get('/products', b2b.productsFindAll);
// app.get('/products/:id', b2b.productFindById);
// app.post('/products', b2b.productAdd);
// app.put('/products/:id', b2b.productUpdate);
// app.post('/search', b2b.searchCompaniesProducts);


//app.delete('/users/:id', b2b.usersDelete);

var httpServer = http.createServer(app);
httpServer.listen(8080);

var httpsServer = https.createServer(credentials, app);
httpsServer.listen(8443);

// app.listen(8080);
console.log('Listening on port 8443...');


// app.listen(8443);
// console.log('Listening on port 8443...');



// #!/usr/bin/env node

// var util = require('util'),
//     http = require('http'),
//     fs = require('fs'),
//     url = require('url'),
//     events = require('events');

// var DEFAULT_PORT = 9000;

// function main(argv) {
//   new HttpServer({
//     'GET': createServlet(StaticServlet),
//     'HEAD': createServlet(StaticServlet)
//   }).start(Number(argv[2]) || DEFAULT_PORT);
// }

// function escapeHtml(value) {
//   return value.toString().
//     replace('<', '&lt;').
//     replace('>', '&gt;').
//     replace('"', '&quot;');
// }

// function createServlet(Class) {
//   var servlet = new Class();
//   return servlet.handleRequest.bind(servlet);
// }

// /**
//  * An Http server implementation that uses a map of methods to decide
//  * action routing.
//  *
//  * @param {Object} Map of method => Handler function
//  */
// function HttpServer(handlers) {
//   this.handlers = handlers;
//   this.server = http.createServer(this.handleRequest_.bind(this));
// }

// HttpServer.prototype.start = function(port) {
//   this.port = port;
//   this.server.listen(port);
//   util.puts('Http Server running at http://localhost:' + port + '/');
// };

// HttpServer.prototype.parseUrl_ = function(urlString) {
//   var parsed = url.parse(urlString);
//   parsed.pathname = url.resolve('/', parsed.pathname);
//   return url.parse(url.format(parsed), true);
// };

// HttpServer.prototype.handleRequest_ = function(req, res) {
//   var logEntry = req.method + ' ' + req.url;
//   if (req.headers['user-agent']) {
//     logEntry += ' ' + req.headers['user-agent'];
//   }
//   util.puts(logEntry);
//   req.url = this.parseUrl_(req.url);
//   var handler = this.handlers[req.method];
//   if (!handler) {
//     res.writeHead(501);
//     res.end();
//   } else {
//     handler.call(this, req, res);
//   }
// };

// /**
//  * Handles static content.
//  */
// function StaticServlet() {}

// StaticServlet.MimeMap = {
//   'txt': 'text/plain',
//   'html': 'text/html',
//   'css': 'text/css',
//   'xml': 'application/xml',
//   'json': 'application/json',
//   'js': 'application/javascript',
//   'jpg': 'image/jpeg',
//   'jpeg': 'image/jpeg',
//   'gif': 'image/gif',
//   'png': 'image/png',
//   'svg': 'image/svg+xml'
// };

// StaticServlet.prototype.handleRequest = function(req, res) {
//   var self = this;
//   var path = ('./' + req.url.pathname).replace('//','/').replace(/%(..)/g, function(match, hex){
//     return String.fromCharCode(parseInt(hex, 16));
//   });
//   var parts = path.split('/');
//   if (parts[parts.length-1].charAt(0) === '.')
//     return self.sendForbidden_(req, res, path);
//   fs.stat(path, function(err, stat) {
//     if (err)
//       return self.sendMissing_(req, res, path);
//     if (stat.isDirectory())
//       return self.sendDirectory_(req, res, path);
//     return self.sendFile_(req, res, path);
//   });
// }

// StaticServlet.prototype.sendError_ = function(req, res, error) {
//   res.writeHead(500, {
//       'Content-Type': 'text/html'
//   });
//   res.write('<!doctype html>\n');
//   res.write('<title>Internal Server Error</title>\n');
//   res.write('<h1>Internal Server Error</h1>');
//   res.write('<pre>' + escapeHtml(util.inspect(error)) + '</pre>');
//   util.puts('500 Internal Server Error');
//   util.puts(util.inspect(error));
// };

// StaticServlet.prototype.sendMissing_ = function(req, res, path) {
//   path = path.substring(1);
//   res.writeHead(404, {
//       'Content-Type': 'text/html'
//   });
//   res.write('<!doctype html>\n');
//   res.write('<title>404 Not Found</title>\n');
//   res.write('<h1>Not Found</h1>');
//   res.write(
//     '<p>The requested URL ' +
//     escapeHtml(path) +
//     ' was not found on this server.</p>'
//   );
//   res.end();
//   util.puts('404 Not Found: ' + path);
// };

// StaticServlet.prototype.sendForbidden_ = function(req, res, path) {
//   path = path.substring(1);
//   res.writeHead(403, {
//       'Content-Type': 'text/html'
//   });
//   res.write('<!doctype html>\n');
//   res.write('<title>403 Forbidden</title>\n');
//   res.write('<h1>Forbidden</h1>');
//   res.write(
//     '<p>You do not have permission to access ' +
//     escapeHtml(path) + ' on this server.</p>'
//   );
//   res.end();
//   util.puts('403 Forbidden: ' + path);
// };

// StaticServlet.prototype.sendRedirect_ = function(req, res, redirectUrl) {
//   res.writeHead(301, {
//       'Content-Type': 'text/html',
//       'Location': redirectUrl
//   });
//   res.write('<!doctype html>\n');
//   res.write('<title>301 Moved Permanently</title>\n');
//   res.write('<h1>Moved Permanently</h1>');
//   res.write(
//     '<p>The document has moved <a href="' +
//     redirectUrl +
//     '">here</a>.</p>'
//   );
//   res.end();
//   util.puts('301 Moved Permanently: ' + redirectUrl);
// };

// StaticServlet.prototype.sendFile_ = function(req, res, path) {
//   var self = this;
//   var file = fs.createReadStream(path);
//   res.writeHead(200, {
//     'Content-Type': StaticServlet.
//       MimeMap[path.split('.').pop()] || 'text/plain'
//   });
//   if (req.method === 'HEAD') {
//     res.end();
//   } else {
//     file.on('data', res.write.bind(res));
//     file.on('close', function() {
//       res.end();
//     });
//     file.on('error', function(error) {
//       self.sendError_(req, res, error);
//     });
//   }
// };

// StaticServlet.prototype.sendDirectory_ = function(req, res, path) {
//   var self = this;
//   if (path.match(/[^\/]$/)) {
//     req.url.pathname += '/';
//     var redirectUrl = url.format(url.parse(url.format(req.url)));
//     return self.sendRedirect_(req, res, redirectUrl);
//   }
//   fs.readdir(path, function(err, files) {
//     if (err)
//       return self.sendError_(req, res, error);

//     if (!files.length)
//       return self.writeDirectoryIndex_(req, res, path, []);

//     var remaining = files.length;
//     files.forEach(function(fileName, index) {
//       fs.stat(path + '/' + fileName, function(err, stat) {
//         if (err)
//           return self.sendError_(req, res, err);
//         if (stat.isDirectory()) {
//           files[index] = fileName + '/';
//         }
//         if (!(--remaining))
//           return self.writeDirectoryIndex_(req, res, path, files);
//       });
//     });
//   });
// };

// StaticServlet.prototype.writeDirectoryIndex_ = function(req, res, path, files) {
//   path = path.substring(1);
//   res.writeHead(200, {
//     'Content-Type': 'text/html'
//   });
//   if (req.method === 'HEAD') {
//     res.end();
//     return;
//   }
//   res.write('<!doctype html>\n');
//   res.write('<title>' + escapeHtml(path) + '</title>\n');
//   res.write('<style>\n');
//   res.write('  ol { list-style-type: none; font-size: 1.2em; }\n');
//   res.write('</style>\n');
//   res.write('<h1>Directory: ' + escapeHtml(path) + '</h1>');
//   res.write('<ol>');
//   files.forEach(function(fileName) {
//     if (fileName.charAt(0) !== '.') {
//       res.write('<li><a href="' +
//         escapeHtml(fileName) + '">' +
//         escapeHtml(fileName) + '</a></li>');
//     }
//   });
//   res.write('</ol>');
//   res.end();
// };

// // Must be last,
// main(process.argv);