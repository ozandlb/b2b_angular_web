describe('Registration', function() {

  var ptor;

  beforeEach(function() {
    browser.get('https://127.0.0.1:8443');
    ptor = protractor.getInstance();

  });





  describe('registration', function() {


    it('should have correct elements at page load', function() {

      var email = element(by.model('regEmail'));
      var password = element(by.model('regPassword'));
      var regButton = element(by.id('registerButton'));


      // placeholders
      expect(email.getAttribute('placeholder')).toEqual('Email');
      expect(password.getAttribute('placeholder')).toEqual('Password');

      // button text
      expect(regButton.getAttribute('value')).toEqual('Register');

    });



    describe('with invalid email "sm$@^fa"', function() {

      beforeEach(function() {
        var registerButton = element(by.id('registerButton'));
        element(by.input('regEmail')).sendKeys('sm$@^fa');
        element(by.input('regPassword')).sendKeys('smsmsm123123123');
        registerButton.click();
      });

      it('shows correct alert status', function() {
        expect(ptor.isElementPresent(by.className('alert-danger'))).toBe(true);
        expect(ptor.isElementPresent(by.className('alert-success'))).toBe(false);
      });

      it('shows correct alert message', function() {
        expect(ptor.findElement(by.css('div.alert div span')).getText()).toBe('Please enter a valid email address');
      });
      
    });


    describe('with invalid email "sm@fa@faaaaaafaaaaa.com"', function() {

      beforeEach(function() {
        var registerButton = element(by.id('registerButton'));
        element(by.input('regEmail')).sendKeys('sm@fa@faaaaaafaaaaa.com');
        element(by.input('regPassword')).sendKeys('smsmsm123123123');
        registerButton.click();
      });

      it('shows correct alert status', function() {
        expect(ptor.isElementPresent(by.className('alert-danger'))).toBe(true);
        expect(ptor.isElementPresent(by.className('alert-success'))).toBe(false);
      });

      it('shows correct alert message', function() {
        expect(ptor.findElement(by.css('div.alert div span')).getText()).toBe('Please enter a valid email address');
      });
      
    });



    describe('with short password', function() {

      beforeEach(function() {
        var registerButton = element(by.id('registerButton'));
        element(by.input('regEmail')).sendKeys('smsmsmsmsm@smsmsm.com');
        element(by.input('regPassword')).sendKeys('asd');
        registerButton.click();
      });

      it('shows correct alert status', function() {
        expect(ptor.isElementPresent(by.className('alert-danger'))).toBe(true);
        expect(ptor.isElementPresent(by.className('alert-success'))).toBe(false);
      });

      it('shows correct alert message', function() {
        expect(ptor.findElement(by.css('div.alert div span')).getText()).toBe('Password must be between 6 and 15 characters');
      });
      
    });


    describe('with long password', function() {

      beforeEach(function() {
        var registerButton = element(by.id('registerButton'));
        element(by.input('regEmail')).sendKeys('smsmsmsmsm@smsmsm.com');
        element(by.input('regPassword')).sendKeys('asd123asd123asd123asd123');
        registerButton.click();
      });

      it('shows correct alert status', function() {
        expect(ptor.isElementPresent(by.className('alert-danger'))).toBe(true);
        expect(ptor.isElementPresent(by.className('alert-success'))).toBe(false);
      });

      it('shows correct alert message', function() {
        expect(ptor.findElement(by.css('div.alert div span')).getText()).toBe('Password must be between 6 and 15 characters');
      });
      
    });



    describe('with existing email', function() {

      beforeEach(function() {
        var registerButton = element(by.id('registerButton'));
        element(by.input('regEmail')).sendKeys('sudeep@jamb2b.com');
        element(by.input('regPassword')).sendKeys('test123123');
        registerButton.click();
      });

      it('shows correct alert status', function() {
        expect(ptor.isElementPresent(by.className('alert-danger'))).toBe(true);
        expect(ptor.isElementPresent(by.className('alert-success'))).toBe(false);
      });

      it('shows correct alert message', function() {
        expect(ptor.findElement(by.css('div.alert div span')).getText()).toBe('Email already exists');
      });
      
    });




  });





});