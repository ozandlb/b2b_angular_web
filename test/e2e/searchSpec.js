describe('Search', function() {

  var ptor;

  beforeEach(function() {
    browser.get('https://127.0.0.1:8443');
    ptor = protractor.getInstance();
		element(by.id('splash')).click();
  });



  it('should have correct elements at page load', function() {

    var searchString = element(by.model('searchString'));
    var searchButton = element(by.id('searchButton'));

    // placeholders
    expect(searchString.getAttribute('placeholder')).toEqual('Search companies, products, locations');

    // button text
    expect(searchButton.getAttribute('value')).toEqual('Search');

  });


  describe('inputting "oo"', function() {


    beforeEach(function() {
      var searchButton = element(by.id('searchButton'));
      element(by.input('searchString')).sendKeys('oo');
      searchButton.click();
    });


    it('url contains "?search=oo"', function() {
      expect(browser.getCurrentUrl()).toContain('?search=oo');
    });


    it('shows correct number of results', function() {
      expect(element.all(by.css('ul li')).count()).toEqual(3);
    });


  });



  describe('inputting "oo us"', function() {


    beforeEach(function() {
      var searchButton = element(by.id('searchButton'));
      element(by.input('searchString')).sendKeys('oo us');
      searchButton.click();
    });


    it('url contains "?search=oo us"', function() {
      expect(browser.getCurrentUrl()).toContain('?search=oo%20us');
    });

    it('shows correct number of results', function() {
      expect(element.all(by.css('ul li')).count()).toEqual(6);
    });


  });



  describe('inputting no result search string "ooo"', function() {


    beforeEach(function() {
      var searchButton = element(by.id('searchButton'));
      element(by.input('searchString')).sendKeys('ooo');
      searchButton.click();
    });


    it('url contains "?search=ooo"', function() {
      expect(browser.getCurrentUrl()).toContain('?search=ooo');
    });

    it('displays "no results"', function() {
      expect(element(by.id('searchResultsBox')).getText()).toBe('No results');
    });

    it('shows no list of results', function() {
      expect(element.all(by.css('ul li')).count()).toEqual(0);
    });


  });


  describe('selecting a search result link', function() {


    beforeEach(function() {
      var searchButton = element(by.id('searchButton'));
      element(by.input('searchString')).sendKeys('oo us');
      searchButton.click();
      var selectedLink = element.all(by.css('ul li a')).get(1);
      // browser.linkText = selectedLink.getText();
      // get and print selected link text
      element.all(by.css('ul li a')).get(1).getText().then(function(text) {
        // console.log(text);
        browser.linkText = text;
      });
      // browser.linkHref = selectedLink.getAttribute('href');
      // get and print selected link href
      element.all(by.css('ul li a')).get(1).getAttribute('href').then(function(text) {
        // console.log(text);
        browser.linkHref = text;
      });
      selectedLink.click();
    });


    it('changes route to /company/:companyid', function() {
      expect(browser.getCurrentUrl()).toMatch(browser.linkHref);
    });

    
    it('has correct content elements', function() {
      expect(browser.findElement(by.id('content')).getText()).toContain(browser.linkText);  
    });


  });







});