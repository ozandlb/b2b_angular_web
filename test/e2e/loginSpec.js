describe('Login', function() {

	beforeEach(function() {
		browser.get('https://127.0.0.1:8443');
	});


	it('at page load should have correct elements', function() {

		var email = element(by.id('email'));
		var password = element(by.id('password'));
		var loginButton = element(by.id('loginButton'));
		var forgotLink = element(by.css('.ForgotLink a'));

		expect(browser.isElementPresent(by.id('alert'))).toBe(false);

		expect(email.getAttribute('placeholder')).toEqual('Email');
		expect(password.getAttribute('placeholder')).toEqual('Password');

		expect(loginButton.getText()).toEqual('Login');
		expect(forgotLink.getText()).toEqual('Forgot');

		expect(forgotLink.getAttribute('href')).toMatch('/#/forgot');

	});






	describe('with missing email', function() {

		beforeEach(function() {

			element(by.id('password')).then(function(elem) {
				elem.sendKeys('user');
			});
			element(by.id('loginButton')).then(function(elem) {
				elem.click();
			});
		});

		it('shows correct alert message', function() {
			element(by.css('.alert-danger div span')).then(function(elem) {
				expect(elem.getText()).toBe('Please enter email address');
			});
		});

	});



	describe('with missing password', function() {

		beforeEach(function() {
			element(by.id('email')).then(function(elem) {
				elem.sendKeys('user@user.com');
			});
			element(by.id('loginButton')).then(function(elem) {
				elem.click();
			});
		});

		it('shows correct alert message', function() {
			element(by.css('.alert-danger div span')).then(function(elem) {
				expect(elem.getText()).toBe('Please enter password');
			});
		});

	});



	describe('with incorrect email address', function() {

		describe('sm$@^fa', function() {

			beforeEach(function() {
				element(by.id('email')).then(function(elem) {
					elem.sendKeys('sm$@^fa');
				});
				element(by.id('password')).then(function(elem) {
					elem.sendKeys('smsmsm123123123');
				});
				element(by.id('loginButton')).then(function(elem) {
					elem.click();
				});
			});

			it('shows correct alert message', function() {
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});

		});

		describe('email@email.c', function() {

			beforeEach(function() {
				element(by.id('email')).then(function(elem) {
					elem.sendKeys('email@email.c');
				});
				element(by.id('password')).then(function(elem) {
					elem.sendKeys('smsmsm123123123');
				});
				element(by.id('loginButton')).then(function(elem) {
					elem.click();
				});
			});

			it('shows correct alert message', function() {
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});

		});

		describe('.email@email.com', function() {
			beforeEach(function() {
				element(by.id('email')).then(function(elem) {
					elem.sendKeys('.email@email.com');
				});
				element(by.id('password')).then(function(elem) {
					elem.sendKeys('smsmsm123123123');
				});
				element(by.id('loginButton')).then(function(elem) {
					elem.click();
				});
			});

			it('shows correct alert message', function() {
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});

		});

		describe('A@b@c@example.com', function() {
			beforeEach(function() {
				element(by.id('email')).then(function(elem) {
					elem.sendKeys('A@b@c@example.com');
				});
				element(by.id('password')).then(function(elem) {
					elem.sendKeys('smsmsm123123123');
				});
				element(by.id('loginButton')).then(function(elem) {
					elem.click();
				});
			});

			it('shows correct alert message', function() {
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});
		});

		describe('email@.com', function() {
			beforeEach(function() {
				element(by.id('email')).then(function(elem) {
					elem.sendKeys('email@.com');
				});
				element(by.id('password')).then(function(elem) {
					elem.sendKeys('smsmsm123123123');
				});
				element(by.id('loginButton')).then(function(elem) {
					elem.click();
				});
			});

			it('shows correct alert message', function() {
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});
		});
	});


	describe('correct email / incorrect password', function() {

		beforeEach(function() {

			element(by.id('email')).then(function(elem) {
				elem.sendKeys('rightuser@rightuser.com');
			});
			element(by.id('password')).then(function(elem) {
				elem.sendKeys('incorrectpassword');
			});
			element(by.id('loginButton')).then(function(elem) {
				elem.click();
			});

		});

		it('shows correct alert message', function() {
			element(by.css('.alert-danger div span')).then(function(elem) {
				expect(elem.getText()).toBe('Login incorrect, please try again');
			});
		});


	});



	describe('with correct email and password', function() {

		beforeEach(function() {

			element(by.id('email')).then(function(elem) {
				elem.sendKeys('sm@jamb2b.com');
			});

			element(by.id('password')).then(function(elem) {
				elem.sendKeys('smsmsm');
			});

			element(by.id('loginButton')).then(function(elem) {
				elem.click();
			});


		});

		afterEach(function() {
			element(by.id('logout')).then(function(elem) {
				elem.click();
			});
		});

		it('does not show alert, changes route to /admin, has title, has add user link', function() {
			expect(browser.isElementPresent(by.css('.alert-success'))).toBe(false);
			expect(browser.isElementPresent(by.css('.alert-danger'))).toBe(false);
			expect(browser.getCurrentUrl()).toMatch(/\/home/);
			expect(browser.findElement(by.id('pagetitle')).getText()).toBe('Admin Home');
			expect(browser.findElement(by.id('adduser')).getText()).toBe('Add New User');
		});

	});




});