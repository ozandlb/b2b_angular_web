describe('Profile', function() {


	describe('going directly to page without logging in', function() {

		beforeEach(function() {
			browser.get('https://127.0.0.1:8443/#/profile');
		});

		it('goes back to login screen', function() {
			expect(browser.getCurrentUrl()).not.toMatch(/\/profile/);
		});

	});


	describe('after logging in', function() {

		beforeEach(function() {
			browser.get('https://127.0.0.1:8443');

			element(by.id('email')).then(function(elem) {
				elem.sendKeys('sm@jamb2b.com');
			});
			element(by.id('password')).then(function(elem) {
				elem.sendKeys('smsmsm');
			});
			element(by.id('loginButton')).then(function(elem) {
				elem.click();
			});

			element(by.id('profile')).then(function(elem) {
				elem.click();
			});

		});


		it('has elements', function() {
			expect(element(by.css('#pagetitle')).getText()).toBe('Profile');
		});

	});


});
