describe('Admin Add User', function() {


	describe('going directly to page without logging in', function() {

		beforeEach(function() {
			browser.get('https://127.0.0.1:8443/#/admin/users/add');
		});

		it('goes back to welcome screen', function() {
			expect(browser.getCurrentUrl()).not.toMatch(/\/home/);
			expect(browser.getCurrentUrl()).toMatch(/https:\/\/127.0.0.1:8443\/#\//);
		});

	});



	describe('after logging in', function() {

		beforeEach(function() {
			browser.get('https://127.0.0.1:8443');

			element(by.id('email')).then(function(elem) {
				elem.sendKeys('sm@jamb2b.com');
			});
			element(by.id('password')).then(function(elem) {
				elem.sendKeys('smsmsm');
			});
			element(by.id('loginButton')).then(function(elem) {
				elem.click();
			});

			element(by.id('adduser')).then(function(elem) {
				elem.click();
			});

		});



		describe('has correct elements', function() {

			it('url, title and input controls', function() {

				expect(browser.getCurrentUrl()).toMatch(/\/admin\/users\/add/);

				element(by.id('pagetitle')).then(function(e) {
					expect(e.getText()).toBe('Add New User');
				});

				element(by.id('firstname')).then(function(e) {
					expect(e.getAttribute('placeholder')).toEqual('First Name');
				});

				element(by.id('lastname')).then(function(e) {
					expect(e.getAttribute('placeholder')).toEqual('Last Name');
				});

			});

		});



		describe('submitting information', function() {

			it('missing firstname', function() {
				element(by.model('lastname')).then(function(e) {
					e.sendKeys('UserLast');
				});
				element(by.model('email')).then(function(e) {
					e.sendKeys('email@email.com');
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter first name');
				});
			});


			it('missing lastname', function() {
				element(by.model('firstname')).then(function(e) {
					e.sendKeys('UserFirst');
				});
				element(by.model('email')).then(function(e) {
					e.sendKeys('email@email.com');
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter last name');
				});
			});


			it('missing email', function() {
				element(by.model('firstname')).then(function(e) {
					e.sendKeys('UserFirst');
				});
				element(by.model('lastname')).then(function(e) {
					e.sendKeys('UserLast');
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter email address');
				});
			});


			it('invalid firstname: User^Invalid', function() {
				element(by.model('firstname')).then(function(e) {
					e.sendKeys('User^Invalid');
				});
				element(by.model('lastname')).then(function(e) {
					e.sendKeys('UserLast');
				});
				element(by.model('email')).then(function(e) {
					e.sendKeys('email@email.com');
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter valid first name');
				});
			});


			it('invalid lastname: User^Invalid', function() {
				element(by.model('firstname')).then(function(e) {
					e.sendKeys('UserFirst');
				});
				element(by.model('lastname')).then(function(e) {
					e.sendKeys('User^Invalid');
				});
				element(by.model('email')).then(function(e) {
					e.sendKeys('email@email.com');
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter valid last name');
				});
			});


			it('invalid email: email@email.', function() {
				element(by.model('firstname')).then(function(e) {
					e.sendKeys('UserValid');
				});
				element(by.model('lastname')).then(function(e) {
					e.sendKeys('UserValid');
				});
				element(by.model('email')).then(function(e) {
					e.sendKeys('email@email.');
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter valid email address');
				});
			});


			it('invalid first name: User^Invalid and then invalid email: email@email.', function() {
				element(by.model('firstname')).then(function(e) {
					e.sendKeys('User^Invalid');
				});
				element(by.model('lastname')).then(function(e) {
					e.sendKeys('UserValid');
				});
				element(by.model('email')).then(function(e) {
					e.sendKeys('email@email.com');
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter valid first name');
				});

				element(by.model('firstname')).then(function(e) {
					e.clear().then(function() {
						e.sendKeys('Ramanujan');
					});
				});

				element(by.model('email')).then(function(e) {
					e.clear().then(function() {
						e.sendKeys('email@email.');
					});
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter valid email address');
				});
			});


			it('all valid inputs but existing user', function() {
				element(by.model('firstname')).then(function(e) {
					e.sendKeys('Sudeep');
				});
				element(by.model('lastname')).then(function(e) {
					e.sendKeys('Mathur');
				});
				element(by.model('email')).then(function(e) {
					e.sendKeys('sm@jamb2b.com');
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Error: User not added, email already exists');
				});
			});


			it('all valid inputs and new user', function() {
				element(by.model('firstname')).then(function(e) {
					e.sendKeys('NewFirst');
				});
				element(by.model('lastname')).then(function(e) {
					e.sendKeys('NewLast');
				});
				element(by.model('email')).then(function(e) {
					e.sendKeys('domokennaressa@gmail.com');
				});
				element(by.id('submitAddUser')).then(function(e) {
					e.click();
				});

				element(by.css('.alert-success div span')).then(function(elem) {
					expect(elem.getText()).toBe('New user added');
				});
			});


		});

	});

});