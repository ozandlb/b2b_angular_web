describe('/forgot', function() {


	function hasForgotAttributes() {
		var email = element(by.id('email'));
		var submitButton = element(by.id('requestResetButton'));
		var cancelLink = element(by.css('.CancelLink a'));

		expect(browser.isElementPresent(by.id('alert'))).toBe(false);
		expect(browser.getCurrentUrl()).toMatch(/\/forgot/);
		expect(email.getAttribute('placeholder')).toEqual('Email');
		expect(submitButton.getText()).toEqual('Submit request');
		expect(cancelLink.getText()).toEqual('Cancel');
	}



	describe('login page link to /forgot', function() {

		beforeEach(function() {
			browser.get('https://127.0.0.1:8443');
			element(by.css('.ForgotLink a')).click();
		});

		it('has forgot email, submit, cancel and URL elements', function() {
			hasForgotAttributes();
		});

	});



	describe('direct access to /forgot', function() {

		beforeEach(function() {
			browser.get('https://127.0.0.1:8443/#/forgot');
		});

		it('has email, submit, cancel and URL elements', function() {
			hasForgotAttributes();
		});


		describe('invalid emails', function() {

			it('sm@', function() {
				element(by.model('email')).sendKeys('sm@');
				element(by.id('requestResetButton')).click();
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});

			it('sm@as', function() {
				element(by.model('email')).sendKeys('sm@as');
				element(by.id('requestResetButton')).click();
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});

			it('s^m@as.com', function() {
				element(by.model('email')).sendKeys('s^m@as.com');
				element(by.id('requestResetButton')).click();
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});

			it('s m@as.com', function() {
				element(by.model('email')).sendKeys('s m@as.com');
				element(by.id('requestResetButton')).click();
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});

			it('sm@as.', function() {
				element(by.model('email')).sendKeys('sm@as.');
				element(by.id('requestResetButton')).click();
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});

			it('s@m@as.com', function() {
				element(by.model('email')).sendKeys('s@m@as.com');
				element(by.id('requestResetButton')).click();
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter a valid email address');
				});
			});

		});


		describe('empty email', function() {

			it('empty', function() {
				element(by.model('email')).sendKeys('');
				element(by.id('requestResetButton')).click();
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter email address');
				});
			});

			it('blank spaces', function() {
				element(by.model('email')).sendKeys('  ');
				element(by.id('requestResetButton')).click();
				element(by.css('.alert-danger div span')).then(function(elem) {
					expect(elem.getText()).toBe('Please enter email address');
				});
			});

		});


		describe('valid but nonexistant email', function() {
			it('domokennaressa@gmail.com', function() {
				element(by.model('email')).sendKeys('domokennaressa@gmail.com');
				element(by.id('requestResetButton')).click();
				expect(browser.getCurrentUrl()).not.toContain('/forgot');
				element(by.css('.alert-success div span')).then(function(elem) {
					expect(elem.getText()).toBe('If this email address exists in our system, you will receive password reset instructions there');
				});
			});
		});


		describe('valid and existing email', function() {
			it('sm@jamb2b.com', function() {
				element(by.model('email')).sendKeys('sm@jamb2b.com');
				element(by.id('requestResetButton')).click();
				expect(browser.getCurrentUrl()).not.toContain('/forgot');
				element(by.css('.alert-success div span')).then(function(elem) {
					expect(elem.getText()).toBe('If this email address exists in our system, you will receive password reset instructions there');
				});
			});
		});


	});


});