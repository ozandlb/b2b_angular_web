'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('b2bwebApp'));
  beforeEach(module('ui.bootstrap'));
  beforeEach(module('ngCookies'));
  beforeEach(module('ngResource'));
  beforeEach(module('ngSanitize'));
  beforeEach(module('ngRoute'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });

  it('should attach an alert array to the scope', function() {
    expect(scope.alerts.length).toBe(0);
  });

});
