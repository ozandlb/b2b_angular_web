'use strict';

angular.module('e2e-mocks', ['ngMockE2E'])
	.run(function($httpBackend) {

//		var baseApiUrl = 'https://127.0.0.1:8445/';

		var reqData = { 'firstname' : 'NewFirst', 'lastname' : 'NewLast', 'email' : 'domokennaressa@gmail.com'};
		var loginResponse = { 'code' : 1, 'data' : 'user added'};

//		$httpBackend.whenPOST(baseApiUrl + 'admin/users', reqData)
		$httpBackend.whenPOST(/^https:\/\/127.0.0.1:8445\/admin\/users\?sessionid=\w+$/, reqData)
			.respond(loginResponse);

// Don't mock the html views
		$httpBackend.whenGET(/views\/\w+.*/).passThrough();

// For everything else, don't mock
		$httpBackend.whenGET(/^\w+.*/).passThrough();
		$httpBackend.whenPOST(/^\w+.*/).passThrough();
		$httpBackend.whenDELETE(/^\w+.*/).passThrough();
	});


angular.module('b2bwebApp').requires.push('e2e-mocks');