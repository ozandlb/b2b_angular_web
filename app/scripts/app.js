'use strict';

angular.module('b2bwebApp', [
    'ui.bootstrap',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'LocalStorageModule',
    'angularFileUpload'
  ])
  .config(['localStorageServiceProvider', function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('ls');
  }])
  .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
			.when('/', {
				templateUrl: 'views/search.html',
				controller: 'SearchCtrl'
			})
			.when('/login', {
				templateUrl: 'views/login.html',
				controller: 'LoginCtrl'
			})
			.when('/logout', {
				templateUrl: 'views/logout.html',
				controller: 'LogoutCtrl'
			})
			.when('/profile', {
				templateUrl: 'views/profile.html',
				controller: 'ProfileCtrl'
			})
      .when('/admin', {
        templateUrl: 'views/admin.html',
        controller: 'AdminCtrl'
      })
			.when('/admin/users/add', {
				templateUrl: 'views/adminadduser.html',
				controller: 'AdminAddUserCtrl'
			})
			.when('/admin/users', {
				templateUrl: 'views/users.html',
				controller: 'UsersCtrl'
			})
			.when('/users/:userId', {
				templateUrl: 'views/userdetails.html',
				controller: 'UserDetailsCtrl'
			})
			.when('/companies', {
				templateUrl: 'views/companies.html',
				controller: 'CompanyCtrl'
			})
      .when('/companies/:companyId', {
        templateUrl: 'views/companydetails.html',
        controller: 'CompanyDetailsCtrl'
      })
			.when('/companies/:companyId/review', {
				templateUrl: 'views/writecompanyreview.html',
				controller: 'WriteCompanyReviewCtrl'
			})
			.when('/products', {
				templateUrl: 'views/products.html',
				controller: 'ProductListCtrl'
			})
			.when('/products/:productId', {
				templateUrl: 'views/productdetails.html',
				controller: 'ProductDetailsCtrl'
			})
			.when('/reviews', {
				templateUrl: 'views/reviews.html',
				controller: 'ReviewsCtrl'
			})
      .when('/forgot', {
        templateUrl: 'views/forgot.html',
        controller: 'ForgotCtrl'
      })
      .when('/reset/:resetHash', {
        templateUrl: 'views/resetpassword.html',
        controller: 'ForgotCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

    // setting to true removes hash from URL, but causes issues with page refreshes
    $locationProvider.html5Mode(false);
  }]);
