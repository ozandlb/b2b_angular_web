'use strict';

angular.module('b2bwebApp')

	.factory('globalService', ['$q', function($q) {


		var isEmpty = function(input) {
			var defer = $q.defer();
			if (typeof input === 'undefined' || input === null || input.length === 0) {
				defer.resolve(true);
			}
			else {
				defer.resolve(false);
			}
			return defer.promise;
		};


		return {
			isEmpty : isEmpty
		};

	}]);
