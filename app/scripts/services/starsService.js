'use strict';

angular.module('b2bwebApp')

	.factory('StarsService', [function() {


		var setAllReviewsStars = function(reviews) {
			for (var i = 0; i<reviews.length; i++) {
				formatReviewStars(reviews[i]);
			}
		};


		var formatReviewStars = function(review) {

			if (typeof review.rating !== 'undefined') {

				review.stars = new Array(5);

				for (var i=0; i<review.stars.length; i++) {
					review.stars[i] = {
						'on' : false,
						'half' : false
					};
				}

				var remainder = parseFloat('0.' + (review.rating + '').split('.')[1]);
//				console.log(remainder);


				switch(Math.floor(review.rating)) {

					case 0:

						if (remainder % 1 >= 0.8) {
							review.stars[0].on = true;
							review.stars[0].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[0].on = false;
							review.stars[0].half = true;
						}
						else {
							review.stars[0].on = false;
							review.stars[0].half = false;
						}

						review.stars[1].on = false;
						review.stars[2].on = false;
						review.stars[3].on = false;
						review.stars[4].on = false;

						review.stars[1].half = false;
						review.stars[2].half = false;
						review.stars[3].half = false;
						review.stars[4].half = false;
						break;


					case 1:

						if (remainder % 1 >= 0.8) {
							review.stars[1].on = true;
							review.stars[1].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[1].on = false;
							review.stars[1].half = true;
						}
						else {
							review.stars[1].on = false;
							review.stars[1].half = false;
						}

						review.stars[0].on = true;
						review.stars[2].on = false;
						review.stars[3].on = false;
						review.stars[4].on = false;

						review.stars[0].half = false;
						review.stars[2].half = false;
						review.stars[3].half = false;
						review.stars[4].half = false;
						break;

					case 2:

						if (remainder % 1 >= 0.8) {
							review.stars[2].on = true;
							review.stars[2].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[2].on = false;
							review.stars[2].half = true;
						}
						else {
							review.stars[2].on = false;
							review.stars[2].half = false;
						}

						review.stars[0].on = true;
						review.stars[1].on = true;
						review.stars[3].on = false;
						review.stars[4].on = false;

						review.stars[0].half = false;
						review.stars[1].half = false;
						review.stars[3].half = false;
						review.stars[4].half = false;
						break;


					case 3:

						if (remainder % 1 >= 0.8) {
							review.stars[3].on = true;
							review.stars[3].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[3].on = false;
							review.stars[3].half = true;
						}
						else {
							review.stars[3].on = false;
							review.stars[3].half = false;
						}

						review.stars[0].on = true;
						review.stars[1].on = true;
						review.stars[2].on = true;
						review.stars[4].on = false;

						review.stars[0].half = false;
						review.stars[1].half = false;
						review.stars[2].half = false;
						review.stars[4].half = false;
						break;


					case 4:

						console.log(remainder);
						if (remainder % 1 >= 0.8) {
							review.stars[4].on = true;
							review.stars[4].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[4].on = false;
							review.stars[4].half = true;
						}
						else {
							review.stars[4].on = false;
							review.stars[4].half = false;
						}

						review.stars[0].on = true;
						review.stars[1].on = true;
						review.stars[2].on = true;
						review.stars[3].on = true;

						review.stars[0].half = false;
						review.stars[1].half = false;
						review.stars[2].half = false;
						review.stars[3].half = false;
						break;


					case 5:

						review.stars[0].on = true;
						review.stars[1].on = true;
						review.stars[2].on = true;
						review.stars[3].on = true;
						review.stars[4].on = true;
						review.stars[0].half = false;
						review.stars[1].half = false;
						review.stars[2].half = false;
						review.stars[3].half = false;
						review.stars[4].half = false;
						break;
				}

			}
		};


		return {
			setAllReviewsStars : setAllReviewsStars,
			formatReviewStars : formatReviewStars
		};


	}]);

