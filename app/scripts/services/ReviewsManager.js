'use strict';

angular.module('b2bwebApp')

	.factory('ReviewsManager', ['b2bGlobals', '$http', 'localStorageService', function(b2bGlobals, $http, localStorageService) {

		this.getReviews = function() {
			var sessiondata = { 'sessionid' : localStorageService.get('session').sessionid };
			return $http({'method': 'GET', 'url' : b2bGlobals.APIServer + '/reviews', 'params' : sessiondata });
		};

		return this;

	}]);