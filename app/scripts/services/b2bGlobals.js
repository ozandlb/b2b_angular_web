'use strict';

angular.module('b2bwebApp')

	.factory('b2bGlobals', function() {

		return {

			APIServer : 'https://127.0.0.1:8445',

			emailRegex : /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,

			maxSessionDurationSecs : 3 * 24 * 60 * 60 // 3 days

		};
	});