'use strict';

angular.module('b2bwebApp')

	.factory('alertsService', [function() {


		var alerts = [];


		var typesOfAlerts = ['danger', 'success'];


		var addAlert = function(alertMsg, alertType) {

			var actualAlertType;

			// if no alert type specified
			if(typeof alertType === 'undefined' || alertType === null) {
				actualAlertType = 'danger';
			}

			// if alert type not one of the official alert types
			else if (typesOfAlerts.indexOf(alertType) === -1) {
				actualAlertType = 'danger';
			}

			else {
				actualAlertType = alertType;
			}

			alerts = [{'msg': alertMsg, 'type': actualAlertType, 'displayed': false}];
			console.log(alerts);
			return alerts;
		};


		var closeAlert = function() { // function(index)
//			alerts.splice(index, 1);
			alerts = [];
			return [];
		};


		var resetAlerts = function(scope) {
			if (alerts.length > 0) {
				alerts = [];
				scope.alerts = [];
			}
		};


		var registerAlertsMethods = function(scope) {


			// alert has been seen before
			if (alerts.length > 0 && alerts[0].displayed === true) {

				// remove alerts
				resetAlerts(scope);
			}

			// alert has not been seen before
			else if (alerts.length > 0 && alerts[0].displayed === false) {

				console.log('alert marked for removal');

				// set alert to seen
				alerts[0].displayed = true;
				scope.alerts = alerts;
			}

			// if alerts is not defined, set to empty array
			else {
				scope.alerts = [];
			}


			scope.closeAlert = function() {
				alerts = [];
				scope.alerts = [];
			};


		};



		return {
			alerts : alerts,
			addAlert : addAlert,
			closerAlert : closeAlert,
			resetAlerts : resetAlerts,
			registerAlertsMethods : registerAlertsMethods
		};

	}]);