'use strict';

angular.module('b2bwebApp')

	.factory('inputsService', ['$q', '$sanitize', 'globalService', function($q, $sanitize, globalService) {

		return {

			checkNameInput : function(inputVal) {

				var sanInputVal = $sanitize(inputVal);
				var defer = $q.defer();

				globalService.isEmpty(sanInputVal).then(function(result) {

					// empty input
					if (result===true) {
						defer.resolve(3);
					}

					else if (result===false) {
						var NameRegEx = /^[a-zA-Z' -]{1,50}$/;

						if(NameRegEx.test(sanInputVal) === false) {
							defer.resolve(2);
						}

						else if(NameRegEx.test(sanInputVal) === true) {
							defer.resolve(1);
						}

						else {
							defer.resolve(4);
						}

					}

				});

				return defer.promise;
			},


			checkEmailInput : function(inputVal) {

				var sanInputVal = $sanitize(inputVal);
				var defer = $q.defer();

				globalService.isEmpty(sanInputVal).then(function(result) {

					// empty input
					if (result===true) {
						defer.resolve(3);
					}

					else if (result===false) {
						var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

						var testres = emailRegEx.test(sanInputVal);

						if(testres === false) {
							defer.resolve(2);
						}

						else if(testres === true) {
							defer.resolve(1);
						}

						else {
							defer.resolve(4);
						}
					}

				});
				return defer.promise;
			}


		};

	}]);
