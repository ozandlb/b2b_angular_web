'use strict';

angular.module('b2bwebApp')

	.factory('apiService', ['$http', 'b2bGlobals', 'localStorageService', function($http, b2bGlobals, localStorageService) {


		var search = function(searchStr) {
			return  $http({'method' : 'POST', 'url' : b2bGlobals.APIServer + '/search', 'data' : { 'searchstr' : searchStr }});
		};


		var getUserList = function() {
			var sessiondata = { 'sessionid' : localStorageService.get('session').sessionid};
			return $http({'method': 'GET', 'url' : b2bGlobals.APIServer + '/users', 'params' : sessiondata});
		};


		var getUserInfo = function(userid) {
//			var sessiondata = { 'sessionid' : localStorageService.get('session').sessionid};
			return $http({'method': 'GET', 'url' : b2bGlobals.APIServer + '/users/'+userid}); // 'params' : sessiondata
		};


		var getCompaniesList = function() {
//			var sessiondata = { 'sessionid' : localStorageService.get('session').sessionid };
			return $http( {'method': 'GET', 'url' : b2bGlobals.APIServer + '/companies'}); // 'params' : sessiondata
		};


		var getCompanyInfo = function(companyid) {
//			var sessiondata = { 'sessionid' : localStorageService.get('session').sessionid };
			return $http({'method': 'GET', 'url' : b2bGlobals.APIServer + '/companies/'+companyid}); // 'params' : sessiondata
		};


		var getProductsList = function() {
//			var sessiondata = { 'sessionid' : localStorageService.get('session').sessionid };
			return $http({'method': 'GET', 'url' : b2bGlobals.APIServer + '/products'}); // 'params' : sessiondata
		};


		var getProductInfo = function(productid) {
//			var sessiondata = { 'sessionid' : localStorageService.get('session').sessionid };
			return $http({'method': 'GET', 'url' : b2bGlobals.APIServer + '/products/'+productid}); // 'params' : sessiondata
		};


		var leaveReview = function(companyId, reviewDataObject) {
			reviewDataObject.sessionid = localStorageService.get('session').sessionid;
			return $http({'method': 'POST', 'url' : b2bGlobals.APIServer + '/companies/'+companyId+'/review', 'data' : reviewDataObject});
		};


		return {
			search : search,
			getUserList : getUserList,
			getUserInfo : getUserInfo,
			getCompaniesList : getCompaniesList,
			getCompanyInfo : getCompanyInfo,
			getProductsList : getProductsList,
			getProductInfo : getProductInfo,
			leaveReview : leaveReview
		};

	}]);
