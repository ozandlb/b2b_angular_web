'use strict';

angular.module('b2bwebApp')

	.factory('RoutingService', [function() {


		var intendedRoute = '';


		var setIntendedRoute = function(iRoute) {
			console.log('setting intended route');
			intendedRoute = iRoute;
		};

		var getIntendedRoute = function() {
			return intendedRoute;
		};


		return {
			setIntendedRoute : setIntendedRoute,
			getIntendedRoute : getIntendedRoute
		};


	}]);

