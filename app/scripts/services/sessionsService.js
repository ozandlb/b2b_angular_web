'use strict';

angular.module('b2bwebApp')

	.factory('sessionsService', ['localStorageService', 'b2bGlobals', '$q', '$http', function(localStorageService, b2bGlobals, $q, $http) {


		var setSessionObject = function(scope) {

			isLocalSessionValid().then(function(result) {

				if (result === true) {

//					scope.logoutUser = function() {
//						deleteSession();
//						$location.path('/login');
//					};

					scope.isLoggedIn = true;
					scope.usersname = localStorageService.get('session').firstname;
				}

				else if (result === false) {
					scope.isLoggedIn = false;
				}

			});
		};


		var saveNewSession = function(session) {

			var sessionObj = {
				'sessionid' : session.sessionid,
				'sessionTimestamp' : new Date().getTime() / 1000,
				'email' : session.email,
				'firstname' : session.firstname,
				'lastname' : session.lastname
			};

			localStorageService.set('session', sessionObj);
		};


		var isLocalSessionValid = function() {

			var deferred = $q.defer();

			doesSessionExist().then(function(result) {

				if (result === false) {
					deferred.resolve(false);
				}

				else {

					// check if too much time has elapsed
					var timeNow = new Date().getTime() / 1000;

					if (timeNow - localStorageService.get('session').sessionTimestamp > b2bGlobals.maxSessionDurationSecs) {
						deferred.resolve(false);
					}

					else if (timeNow - localStorageService.get('session').sessionTimestamp < b2bGlobals.maxSessionDurationSecs) {
						deferred.resolve(true);
					}
				}

			});
			return deferred.promise;
		};


		var doesSessionExist = function() {

			var deferred = $q.defer();

			if (localStorageService.get('session') === null || typeof localStorageService.get('session') === 'undefined' ||
				localStorageService.get('session').sessionid === null || typeof localStorageService.get('session').sessionid === 'undefined' ||
				localStorageService.get('session').sessionTimestamp === null || typeof localStorageService.get('session').sessionTimestamp === 'undefined') {
				deferred.resolve(false);
			}

			else {
				deferred.resolve(true);
			}

			return deferred.promise;
		};



		var isServerSessionValid = function() {
			return $http({'method': 'GET', 'url' : b2bGlobals.APIServer + '/session', 'params' : {'sessionid' : localStorageService.get('session').sessionid}});
		};


		var resetSession = function() {
			localStorageService.set('session', null);
		};


		var deleteSession = function() {

			// delete database record of session
			var sessionId = localStorageService.get('session').sessionid;

			var dataObj = {
				'sessionId' : sessionId
			};

			$http({
				'method' : 'DELETE',
				'url' : b2bGlobals.APIServer + '/session',
				'data' : dataObj
			})
				.success(
				function(delSessionResult) {
					console.log(delSessionResult);
				});

			// delete cookie
			localStorageService.remove('session');

		};


		return {
			isServerSessionValid : isServerSessionValid,
			saveNewSession : saveNewSession,
			isLocalSessionValid : isLocalSessionValid,
			deleteSession : deleteSession,
			setSessionObject : setSessionObject,
			resetSession : resetSession
		};

	}]);