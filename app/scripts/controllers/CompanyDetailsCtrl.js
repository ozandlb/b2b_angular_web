'use strict';

angular.module('b2bwebApp')

	.controller('CompanyDetailsCtrl', ['$scope', '$routeParams', 'sessionsService', 'apiService', 'alertsService', '$location', 'RoutingService', 'StarsService', function($scope, $routeParams, sessionsService, apiService, alertsService, $location, RoutingService, StarsService) {

		$scope.showLogoAtTop = true;

		alertsService.registerAlertsMethods($scope);

		sessionsService.setSessionObject($scope);

		function getCompanyDetails(companyid) {
			apiService.getCompanyInfo(companyid).then(function(result) {

				$scope.company = result.data.data.company;
				$scope.reviews = result.data.data.reviews;


				for (var i=0;i<$scope.reviews.length;i++) {

					// format titles as safe HTML
//					$scope.reviews[i].subject = $sce.parseAsHtml($scope.reviews[i].subject);
//					$scope.reviews[i].text = $sce.parseAsHtml($scope.reviews[i].text);


					$scope.reviews[i].timestampFormatted = moment($scope.reviews[i].timestamp).format('MMMM, D YYYY h:mma');
				  StarsService.formatReviewStars($scope.reviews[i]);
				}

				$scope.products = result.data.data.products;
			});
		}


		getCompanyDetails($routeParams.companyId);



		var initializePage = function() {
			$scope.companyId = $routeParams.companyId;
		};

		initializePage();


		var formatReviewStarsOLD = function(review) {

			if (typeof review.rating !== 'undefined') {

				review.stars = new Array(5);

				for (var i=0; i<review.stars.length; i++) {
					review.stars[i] = {
						'on' : false,
						'half' : false
					};
				}

				var remainder = parseFloat('0.' + (review.rating + '').split('.')[1]);
//				console.log(remainder);


				switch(Math.floor(review.rating)) {

					case 0:

						if (remainder % 1 >= 0.8) {
							review.stars[0].on = true;
							review.stars[0].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[0].on = false;
							review.stars[0].half = true;
						}
						else {
							review.stars[0].on = false;
							review.stars[0].half = false;
						}

						review.stars[1].on = false;
						review.stars[2].on = false;
						review.stars[3].on = false;
						review.stars[4].on = false;

						review.stars[1].half = false;
						review.stars[2].half = false;
						review.stars[3].half = false;
						review.stars[4].half = false;
						break;


					case 1:

						if (remainder % 1 >= 0.8) {
							review.stars[1].on = true;
							review.stars[1].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[1].on = false;
							review.stars[1].half = true;
						}
						else {
							review.stars[1].on = false;
							review.stars[1].half = false;
						}

						review.stars[0].on = true;
						review.stars[2].on = false;
						review.stars[3].on = false;
						review.stars[4].on = false;

						review.stars[0].half = false;
						review.stars[2].half = false;
						review.stars[3].half = false;
						review.stars[4].half = false;
						break;

					case 2:

						if (remainder % 1 >= 0.8) {
							review.stars[2].on = true;
							review.stars[2].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[2].on = false;
							review.stars[2].half = true;
						}
						else {
							review.stars[2].on = false;
							review.stars[2].half = false;
						}

						review.stars[0].on = true;
						review.stars[1].on = true;
						review.stars[3].on = false;
						review.stars[4].on = false;

						review.stars[0].half = false;
						review.stars[1].half = false;
						review.stars[3].half = false;
						review.stars[4].half = false;
						break;


					case 3:

						if (remainder % 1 >= 0.8) {
							review.stars[3].on = true;
							review.stars[3].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[3].on = false;
							review.stars[3].half = true;
						}
						else {
							review.stars[3].on = false;
							review.stars[3].half = false;
						}

						review.stars[0].on = true;
						review.stars[1].on = true;
						review.stars[2].on = true;
						review.stars[4].on = false;

						review.stars[0].half = false;
						review.stars[1].half = false;
						review.stars[2].half = false;
						review.stars[4].half = false;
						break;


					case 4:

						console.log(remainder);
						if (remainder % 1 >= 0.8) {
							review.stars[4].on = true;
							review.stars[4].half = false;
						}
						else if (remainder % 1 >= 0.2) {
							review.stars[4].on = false;
							review.stars[4].half = true;
						}
						else {
							review.stars[4].on = false;
							review.stars[4].half = false;
						}

						review.stars[0].on = true;
						review.stars[1].on = true;
						review.stars[2].on = true;
						review.stars[3].on = true;

						review.stars[0].half = false;
						review.stars[1].half = false;
						review.stars[2].half = false;
						review.stars[3].half = false;
						break;


					case 5:

						review.stars[0].on = true;
						review.stars[1].on = true;
						review.stars[2].on = true;
						review.stars[3].on = true;
						review.stars[4].on = true;
						review.stars[0].half = false;
						review.stars[1].half = false;
						review.stars[2].half = false;
						review.stars[3].half = false;
						review.stars[4].half = false;
						break;
				}

			}


		};


		$scope.writeCompanyReview = function() {

			// check local session
			sessionsService.isLocalSessionValid().then(function(result) {

				var intendedRoute = '/companies/' + $routeParams.companyId + '/review';

				// local session is valid
				if (result === true) {
					sessionsService.isServerSessionValid().then(function(result2) {

						// if server session is valid
						if(result2.data.code === 1) {
							$location.path(intendedRoute);
						}

						// server code is invalid
						else {
							RoutingService.setIntendedRoute(intendedRoute);
							$location.path('/login');
						}

					});
				}

				// if local session is not valid
				else {
					RoutingService.setIntendedRoute(intendedRoute);
					$location.path('/login');
				}

			});


		};



//		sessionsService.isLocalSessionValid().then(function(result) {
//			if (result === true) {
//				getCompanyDetails($routeParams.companyId);
//			}
//			else {
//				console.log('session is not valid');
//				$location.path('/welcome');
//			}
//		});


//		$scope.triggerFileUpload = function($files) {
//
//			console.log($scope.myModelObj);
//			// return;
//
//			//$files: an array of files selected, each file has name, size, and type.
//			for (var i = 0; i < $files.length; i++) {
//				var file = $files[i];
//				$scope.upload = $upload.upload({
//					url: b2bGlobals.APIServer + '/upload', //upload.php script, node.js route, or servlet url
//					method: 'POST', // 'PUT' or 'POST',
//					// headers: {'header-key': 'header-value'},
//					// withCredentials: true,
//					data: {myObj: $scope.myModelObj},
//					file: file, // or list of files: $files for html5 only
//					/* set the file formData name ('Content-Desposition'). Default is 'file' */
//					//fileFormDataName: myFile, //or a list of names for multiple files (html5).
//					/* customize how data is added to formData. See #40#issuecomment-28612000 for sample code */
//					//formDataAppender: function(formData, key, val){}
//				}).progress(function(evt) {
//						console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
//					}).success(function(data) { // status, headers, config << other params
//						// file is uploaded successfully
//						console.log(data);
//					});
//				//.error(...)
//				//.then(success, error, progress);
//				//.xhr(function(xhr){xhr.upload.addEventListener(...)})// access and attach any event listener to XMLHttpRequest.
//			}
//			/* alternative way of uploading, send the file binary with the file's content-type.
//			 Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed.
//			 It could also be used to monitor the progress of a normal http post/put request with large data*/
//			// $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
//		};
//



	}]);