'use strict';

angular.module('b2bwebApp')

	.controller('AdminAddUserCtrl', ['$scope', 'alertsService', '$sanitize', 'sessionsService', '$location', 'inputsService', '$http', 'b2bGlobals', 'localStorageService', function($scope, alertsService, $sanitize, sessionsService, $location, inputsService, $http, b2bGlobals, localStorageService) {


		$scope.showLogoAtTop = true;

		sessionsService.setSessionObject($scope);


		function checkSessions() {

			sessionsService.isLocalSessionValid().then(function(result) {


				console.log('local session is valid');
				console.log(result);
				// local session is valid
				if (result === true) {
					sessionsService.isServerSessionValid().then(function(result2) {

						console.log(result2.data);

						// server session is not valid
						if (result2.data.code !== 1) {
							$location.path('/welcome');
						}
					});
				}

				else{
					$location.path('/welcome');
				}
			});

		}


		checkSessions();


		$scope.inviteAuthenticatedUser = function() {

			alertsService.resetAlerts();

			// check both emails are the same
			if ($scope.email !== $scope.email2) {
				return;
			}

			// check firstname input
			inputsService.checkNameInput($scope.firstname).then(function(res1) {

				// firstname input is empty
				if(res1 === 3) {
					alertsService.addAlert('Please enter first name');
					return;
				}

				// firstname input is not valid
				else if(res1 === 2) {
					alertsService.addAlert('Please enter valid first name');
					return;
				}

				// unknown error
				else if(res1 !== 1) {
					alertsService.addAlert('Error');
					return;
				}


				// firstname is valid
				else if (res1 === 1) {

					// check last name input
					inputsService.checkNameInput($scope.lastname).then(function(res2) {

						// lastname input is empty
						if(res2 === 3) {
							alertsService.addAlert('Please enter last name');
							return;
						}

						// lastname input is not valid
						else if(res2 === 2) {
							alertsService.addAlert('Please enter valid last name');
							return;
						}

						// unknown error
						else if(res2 !== 1) {
							alertsService.addAlert('Error');
							return;
						}

						// last name is valid
						else if (res2 === 1) {

							// check email input
							inputsService.checkEmailInput($scope.email).then(function(res3) {

								// email input is empty
								if(res3 === 3) {
									alertsService.addAlert('Please enter email address');
									return;
								}

								// email input is not valid
								else if(res3 === 2) {
									alertsService.addAlert('Please enter valid email address');
									return;
								}

								// unknown error
								else if(res3 !== 1) {
									alertsService.addAlert('Error');
									return;
								}

								// email input is valid
								else if (res3 === 1) {


									var apiData = {
										'firstname' : $sanitize($scope.firstname),
										'lastname' : $sanitize($scope.lastname),
										'email' : $sanitize($scope.email)
									};

									$http(
										{
											'method' : 'POST',
											'url' : b2bGlobals.APIServer + '/admin/users' + '?sessionid=' + localStorageService.get('session').sessionid,
											'data' : apiData
										}
									)
										.success(
										function(result) {

											console.log(result);

											// user already exists
											if(result.code === 222) {
												alertsService.addAlert('Error: User not added, email already exists', 'danger');
											}

											else if(result.code === 1) {
												alertsService.addAlert('New user added', 'success');
											}

											else {
												alertsService.addAlert('Error', 'danger');
											}

										}
									);
								}
							});
						}
					});
				}
			});



			$scope.alerts = alertsService.alerts;

			$scope.closeAlert = function(index) {
				alertsService.closeAlert(index);
			};


		};

	}]);