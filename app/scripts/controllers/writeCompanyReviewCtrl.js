'use strict';

angular.module('b2bwebApp')

	.controller('WriteCompanyReviewCtrl', ['$scope', 'apiService', '$routeParams', '$q', 'alertsService', '$location', 'sessionsService', function($scope, apiService, $routeParams, $q, alertsService, $location, sessionsService) {



		// check if user is logged in
		sessionsService.isLocalSessionValid().then(function(result) {



			// local session is valid
			if (result === true) {
				sessionsService.isServerSessionValid().then(function(result2) {

				});
			}

			else {
				$location.path('/login');

			}

		});


		sessionsService.setSessionObject($scope);


		$scope.showLogoAtTop = true;

		alertsService.registerAlertsMethods($scope);


		var getCompanyDetails = function(companyid) {
			apiService.getCompanyInfo(companyid).then(function(result) {

				$scope.company = result.data.data.company;

				$scope.products = result.data.data.products;
			});
		};

		getCompanyDetails($routeParams.companyId);



		var initializePage = function() {
			$scope.reviewText = '';
			$scope.reviewTitle = '';
			$scope.starCount = 0;
			$scope.starWord = 'stars';
			$scope.starInputted = false;
			$scope.allValuesNotEntered = true;

			$scope.stars = new Array(5);

			for (var i=0; i<$scope.stars.length; i++) {
				$scope.stars[i] = {
					'on' : false,
					'half' : false
				};
			}
		};


		initializePage();




		var areAllInputsEntered = function() {

			var deferred = $q.defer();

			if (
				($scope.reviewText.trim().length === 0) ||
					($scope.reviewTitle.trim().length === 0) ||
					($scope.positiveReview !== false && $scope.positiveReview!== true) ||
					($scope.starInputted === false)
				) {
				deferred.resolve(false);
			}

			else {
				deferred.resolve(true);
			}

			return deferred.promise;
		};





		$scope.checkOkToSubmit = function() {

			areAllInputsEntered().then(function(result) {

				if (result === true || result === false) {
					$scope.allValuesNotEntered = !result;
				}

				else {
					$scope.allValuesNotEntered = true;
				}

			});

		};



		$scope.toggleStar = function(starNumber) {

			$scope.starInputted = true;

			if (
				$scope.stars[starNumber].on === true &&
					$scope.stars[starNumber].half === false &&
					(starNumber === 4 || (starNumber !== 4 && $scope.stars[starNumber+1].on === false) )
				) {
				$scope.stars[starNumber].half = true;
				$scope.starCount = $scope.starCount - 0.5;
				$scope.starWord = 'stars';

			}


			else if ($scope.stars[starNumber].on === true && $scope.stars[starNumber].half === true) {
				$scope.starCount = 0;
				$scope.starWord = 'stars';
				$scope.stars[0].on = false;
				$scope.stars[0].half = false;
				$scope.stars[1].on = false;
				$scope.stars[1].half = false;
				$scope.stars[2].on = false;
				$scope.stars[2].half = false;
				$scope.stars[3].on = false;
				$scope.stars[3].half = false;
				$scope.stars[4].on = false;
				$scope.stars[4].half = false;

			}

			else {

				$scope.starCount = starNumber + 1;

				switch(starNumber) {
					case 0:
						$scope.starWord = 'star';
						$scope.stars[0].on = true;
						$scope.stars[0].half = false;
						$scope.stars[1].on = false;
						$scope.stars[1].half = false;
						$scope.stars[2].on = false;
						$scope.stars[2].half = false;
						$scope.stars[3].on = false;
						$scope.stars[3].half = false;
						$scope.stars[4].on = false;
						$scope.stars[4].half = false;
						break;
					case 1:
						$scope.starWord = 'stars';
						$scope.stars[0].on = true;
						$scope.stars[0].half = false;
						$scope.stars[1].on = true;
						$scope.stars[1].half = false;
						$scope.stars[2].on = false;
						$scope.stars[2].half = false;
						$scope.stars[3].on = false;
						$scope.stars[3].half = false;
						$scope.stars[4].on = false;
						$scope.stars[4].half = false;
						break;
					case 2:
						$scope.starWord = 'stars';
						$scope.stars[0].on = true;
						$scope.stars[0].half = false;
						$scope.stars[1].on = true;
						$scope.stars[1].half = false;
						$scope.stars[2].on = true;
						$scope.stars[2].half = false;
						$scope.stars[3].on = false;
						$scope.stars[3].half = false;
						$scope.stars[4].on = false;
						$scope.stars[4].half = false;
						break;
					case 3:
						$scope.starWord = 'stars';
						$scope.stars[0].on = true;
						$scope.stars[0].half = false;
						$scope.stars[1].on = true;
						$scope.stars[1].half = false;
						$scope.stars[2].on = true;
						$scope.stars[2].half = false;
						$scope.stars[3].on = true;
						$scope.stars[3].half = false;
						$scope.stars[4].on = false;
						$scope.stars[4].half = false;
						break;
					case 4:
						$scope.starWord = 'stars';
						$scope.stars[0].on = true;
						$scope.stars[0].half = false;
						$scope.stars[1].on = true;
						$scope.stars[1].half = false;
						$scope.stars[2].on = true;
						$scope.stars[2].half = false;
						$scope.stars[3].on = true;
						$scope.stars[3].half = false;
						$scope.stars[4].on = true;
						$scope.stars[4].half = false;
						break;
				}

			}

			$scope.checkOkToSubmit();

		};




		$scope.setPositiveReview = function(tOrF) {
			if (tOrF === true || tOrF === false) {
				$scope.positiveReview = tOrF;
				$scope.checkOkToSubmit();
//				console.log($scope.positiveReview);
			}
		};


		// issue API call to send review details
		$scope.sendReview = function() {

			// if any of the required inputs is empty
			areAllInputsEntered().then(function(result) {

				// all inputs are not entered
				if (result === false) {
					console.log('submission rejected');
				}

				// all inputs are entered
				else if (result === true) {

					apiService.leaveReview($scope.company.id,
						{
							'writtenReviewTitle' : $scope.reviewTitle,
							'writtenReviewText' : $scope.reviewText,
							'rating' : $scope.starCount,
							'positive_review' : $scope.positiveReview
						}
					)
						.then(function(result) {
							// review successfully added
							if (result.data.code === 1) {
								console.log('review successfully added');
								alertsService.addAlert('Successfully added review!', 'success');
								$location.path('/companies/' + $scope.company.id);
							}

							else {
								alertsService.addAlert('Problem adding review', 'danger');
								console.log('problem adding review');
							}

						}
					);

				}

				else {
					console.log('submission error');
				}

			});

		};





	}]);
