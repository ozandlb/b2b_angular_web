'use strict';

angular.module('b2bwebApp')

	.controller('UserDetailsCtrl', ['$scope', 'apiService', '$routeParams', '$location', 'sessionsService', function($scope, apiService, $routeParams, $location, sessionsService) {

		$scope.showLogoAtTop = true;

		sessionsService.setSessionObject($scope);

		function getUserDetails(userid) {
			apiService.getUserInfo(userid).then(function(result) {

				if (result.data.code === 900) {
					$location.path('/welcome');
					return;
				}

				$scope.user = result.data.data.user;
				$scope.reviews = result.data.data.reviews;
				for (var i=0;i<$scope.reviews.length;i++) {
					$scope.reviews[i].timestampFormatted = moment($scope.reviews[i].timestamp).format('MMMM, D YYYY h:mma');
				}
			});
		}


		getUserDetails($routeParams.userId);

//		sessionsService.isLocalSessionValid().then(function(result) {
//			if (result === true) {
//				getUserDetails($routeParams.userId);
//			}
//			else {
//				console.log('session is not valid');
//				$location.path('/welcome');
//			}
//		});


	}]);