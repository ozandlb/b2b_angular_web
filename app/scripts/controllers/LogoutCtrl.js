/*jshint loopfunc: true */
'use strict';

angular.module('b2bwebApp')

	.controller('LogoutCtrl', ['sessionsService', '$location', '$interval', 'alertsService', function(sessionsService, $location, $interval, alertsService) {


		var logoutUser = function() {
			$interval(function() {
					sessionsService.resetSession();
					alertsService.addAlert('Logged out', 'success');
					$location.path('/search');
				},
				1000,
				1);
		};

		logoutUser();


	}]);
