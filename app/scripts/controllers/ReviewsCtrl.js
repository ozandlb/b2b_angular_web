/*jshint loopfunc: true */
'use strict';

angular.module('b2bwebApp')

	.controller('ReviewsCtrl', ['$scope', 'ReviewsManager', 'sessionsService', '$location', function($scope, ReviewsManager, sessionsService, $location) {

		$scope.showLogoAtTop = true;

		sessionsService.setSessionObject($scope);

		function getReviews() {
			ReviewsManager.getReviews().then(function(result) {

				$scope.reviews = result.data.data;
				for (var i=0;i<$scope.reviews.length;i++) {
					$scope.reviews[i].timestampFormatted = moment($scope.reviews[i].timestamp).format('MMMM, D YYYY h:mma');
				}
			});
		}


		sessionsService.isLocalSessionValid().then(function(result) {
			if (result === true) {
				getReviews();
			}
			else {
				console.log('session is not valid');
				$location.path('/welcome');
			}
		});

	}]);