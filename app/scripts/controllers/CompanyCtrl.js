/*jshint loopfunc: true */
'use strict';

angular.module('b2bwebApp')

  .controller('CompanyCtrl', ['$scope', 'apiService', '$location', 'sessionsService', function($scope, apiService, $location, sessionsService) {


		$scope.showLogoAtTop = true;

		sessionsService.setSessionObject($scope);

		function getCompanies() {
			apiService.getCompaniesList().then(function(result) {

				if (result.data.code === 900) {
					$location.path('/welcome');
				}

				else {
					$scope.companies = result.data.data;
				}
			});
		}

		getCompanies();

//		sessionsService.isLocalSessionValid().then(function(result) {
//			if (result === true) {
//				getCompanies();
//			}
//			else {
//				console.log('session is not valid');
//				$location.path('/welcome');
//			}
//		});

	}]);