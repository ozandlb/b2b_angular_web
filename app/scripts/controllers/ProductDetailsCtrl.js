'use strict';

angular.module('b2bwebApp')

	.controller('ProductDetailsCtrl', ['$scope', 'apiService', '$routeParams', '$location', 'sessionsService', 'StarsService', function($scope, apiService, $routeParams, $location, sessionsService, StarsService) {

		$scope.showLogoAtTop = true;

		sessionsService.setSessionObject($scope);

		function getProductDetails(companyid) {
			apiService.getProductInfo(companyid).then(function(result) {

				console.log(result);
				if (result.data.code === 900) {
					$location.path('/welcome');
				}
				else {
					$scope.product = result.data.data.product;
					$scope.reviews = result.data.data.reviews;
					for (var i=0;i<$scope.reviews.length;i++) {
						$scope.reviews[i].timestampFormatted = moment($scope.reviews[i].timestamp).format('MMMM, D YYYY h:mma');
	        	StarsService.formatReviewStars($scope.reviews[i]);
					}
				}
			});
		}


		getProductDetails($routeParams.productId);

//		sessionsService.isLocalSessionValid().then(function(result) {
//			if (result === true) {
//				getProductDetails($routeParams.productId);
//			}
//			else {
//				console.log('session is not valid');
//				$location.path('/welcome');
//			}
//		});


//		$scope.triggerFileUpload = function($files) {
//
//			console.log($scope.myModelObj);
//			// return;
//
//			//$files: an array of files selected, each file has name, size, and type.
//			for (var i = 0; i < $files.length; i++) {
//				var file = $files[i];
//				$scope.upload = $upload.upload({
//					url: b2bGlobals.APIServer + '/upload', //upload.php script, node.js route, or servlet url
//					method: 'POST', // 'PUT' or 'POST',
//					// headers: {'header-key': 'header-value'},
//					// withCredentials: true,
//					data: {myObj: $scope.myModelObj},
//					file: file, // or list of files: $files for html5 only
//					/* set the file formData name ('Content-Desposition'). Default is 'file' */
//					//fileFormDataName: myFile, //or a list of names for multiple files (html5).
//					/* customize how data is added to formData. See #40#issuecomment-28612000 for sample code */
//					//formDataAppender: function(formData, key, val){}
//				}).progress(function(evt) {
//						console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
//					}).success(function(data) { // status, headers, config << other params
//						// file is uploaded successfully
//						console.log(data);
//					});
//				//.error(...)
//				//.then(success, error, progress);
//				//.xhr(function(xhr){xhr.upload.addEventListener(...)})// access and attach any event listener to XMLHttpRequest.
//			}
//			/* alternative way of uploading, send the file binary with the file's content-type.
//			 Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed.
//			 It could also be used to monitor the progress of a normal http post/put request with large data*/
//			// $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
//		};
//



	}]);