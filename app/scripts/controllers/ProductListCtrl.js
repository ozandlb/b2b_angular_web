/*jshint loopfunc: true */
'use strict';

angular.module('b2bwebApp')

	.controller('ProductListCtrl', ['$scope', 'apiService', '$location', 'sessionsService', function($scope, apiService, $location, sessionsService) {

		$scope.showLogoAtTop = true;

		sessionsService.setSessionObject($scope);

		function getProducts() {
			apiService.getProductsList().then(function(result) {
				if (result.data.code === 900) {
					$location.path('/welcome');
				}
				else {
					$scope.products = result.data.data;
				}
			});
		}

		getProducts();

//		sessionsService.isLocalSessionValid().then(function(result) {
//			if (result === true) {
//				getProducts();
//			}
//			else {
//				console.log('session is not valid');
//				$location.path('/welcome');
//			}
//		});


	}]);