'use strict';

angular.module('b2bwebApp')

	.controller('UsersCtrl', ['$scope', 'apiService', '$location', 'sessionsService', function($scope, apiService, $location, sessionsService) {

		$scope.showLogoAtTop = true;

		sessionsService.setSessionObject($scope);

		sessionsService.isLocalSessionValid().then(function(result) {
			if (result === true) {
				getUsers();
			}
			else {
				console.log('session is not valid');
				$location.path('/welcome');
			}
		});


		function getUsers() {
			apiService.getUserList().then(function(result) {

				console.log(result.data.code);

				if (result.data.code === 900) {
					$location.path('/welcome');
					return;
				}

				else {
					$scope.users = result.data.data;
				}
				//$scope.$apply(); // if using $.ajax
			});
		}


	}]);