'use strict';

angular.module('b2bwebApp')

	.controller('LoginCtrl', ['$scope', '$http', '$location', 'b2bGlobals', '$cookieStore', '$sanitize', 'localStorageService', 'sessionsService', 'alertsService', 'RoutingService', function($scope, $http, $location, b2bGlobals, $cookieStore, $sanitize, localStorageService, sessionsService, alertsService, RoutingService) {

		alertsService.registerAlertsMethods($scope);

		sessionsService.resetSession();




	}]);