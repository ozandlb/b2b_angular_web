'use strict';

angular.module('b2bwebApp')

	.controller('SearchCtrl', ['$scope', '$http', '$location', 'b2bGlobals', '$cookieStore', '$sanitize', 'localStorageService', 'sessionsService', 'alertsService', 'apiService', function ($scope, $http, $location, b2bGlobals, $cookieStore, $sanitize, localStorageService, sessionsService, alertsService, apiService) {

		alertsService.registerAlertsMethods($scope);

		sessionsService.setSessionObject($scope);

// ************************************************************************
// ************************************************************************
//            LOGIN METHODS
// ************************************************************************
// ************************************************************************

		$scope.checkSession = function() {

			var validSession = sessionsService.isSessionValid();

			if (validSession === true) {
				$location.path('/admin');
			}

			else if (validSession === false) {
				$location.path('/welcome');
			}

		};


		$scope.attemptLogin = function() {

			if ($scope.alerts.length > 0) {
				$scope.alerts = [];
			}

			var formEmail = $sanitize($scope.email);
			var formPassword = $sanitize($scope.password);


			// error check inputs


			if (formEmail === '' || formEmail === null || typeof formEmail === 'undefined') {
				alertsService.addAlert('Please enter email address', 'danger');
				return;
			}


			var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var passwordRegEx = /^.{6,15}$/;

			if (!emailRegEx.test(formEmail)) {
				alertsService.addAlert('Please enter a valid email address', 'danger');
				return;
			}



			if (formPassword === '' || formPassword === null || typeof formPassword === 'undefined') {
				alertsService.addAlert('Please enter password', 'danger');
				return;
			}


			if (!passwordRegEx.test(formPassword)) {
				alertsService.addAlert('Login incorrect, please try again', 'danger');
				return;
			}


			var loginData = {
				'email' : formEmail,
				'pword' : formPassword
			};

			// login
			$http({
				'method' : 'POST',
				'url' : b2bGlobals.APIServer + '/login',
				'data' : loginData
			})
				.success(
				function(loginResult) {

					console.log('function success');

					console.log(loginResult.code);

					// login is successful
					if (loginResult.code === 1) {

						console.log('login successful');

						// build session object
						var sessObj = {
							'session' : loginResult.data.sessionid,
							'email' : formEmail,
							'firstname' : loginResult.data.firstname,
							'lastname' : loginResult.data.lastname
						};

						// save session key in localStorage
						// console.log(loginResult.data.sessionid);
						sessionsService.saveNewSession(sessObj);
						// localStorageService.set('sessionid', loginResult.data.session);

						$location.path('/admin');

						// apply as not using angular's $http
						$scope.$apply();

					}

					else {
						alertsService.addAlert('Login incorrect, please try again', 'danger');

						// apply as not using angular's $http
						$scope.$apply();

					}

				}
			);
		};




// ************************************************************************
// ************************************************************************
//            REGISTER METHODS
// ************************************************************************
// ************************************************************************



		$scope.registerNewUser = function() {



			if ($scope.alerts.length > 0) {
				$scope.alerts = [];
			}


			var sanitFormRegEmail = $sanitize($scope.regEmail);
			var sanitFormRegPassword = $sanitize($scope.regPassword);



			// error check inputs
			var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var passwordRegEx = /^.{6,15}$/;


			if (sanitFormRegEmail === '' || sanitFormRegEmail === null || typeof sanitFormRegEmail === 'undefined') {
				alertsService.addAlert('Please enter email address', 'danger');
				return;
			}


			if (!emailRegEx.test(sanitFormRegEmail)) {
				alertsService.addAlert('Please enter a valid email address', 'danger');
				return;
			}


			if (!passwordRegEx.test(sanitFormRegPassword)) {
				alertsService.addAlert('Password must be between 6 and 15 characters', 'danger');
				return;
			}


			if (sanitFormRegPassword === '' || sanitFormRegPassword === null || typeof sanitFormRegPassword === 'undefined') {
				alertsService.addAlert('Please enter password', 'danger');
				return;
			}


			console.log(sanitFormRegEmail);
			console.log(sanitFormRegPassword);





			var registrationData = {
				'email' : sanitFormRegEmail,
				'pword' : sanitFormRegPassword
			};


			$http({
				'method' : 'POST',
				'url' : b2bGlobals.APIServer + '/users',
				'data' : registrationData

			})
				.success(
				function(registrationResult) {
					console.log(registrationResult);


					if (registrationResult.code === 1) {
						$location.path('/admin');
					}

					else if (registrationResult.code === 4) {
						alertsService.addAlert('Email already exists', 'danger');

						$scope.$apply();
					}

					else {
						alertsService.addAlert('Error', 'danger');
					}

					// because not using angular $http
					$scope.$apply();

				}
			);

		};



// ************************************************************************
// ************************************************************************
//            SEARCH METHODS
// ************************************************************************
// ************************************************************************


		$scope.searchResults = [];



		$scope.submitSearch = function(searchStr) {

			if (typeof searchStr === 'undefined') {
				return;
			}

			else if (searchStr.trim().length === 0) {
				console.log('trimmed length is zero');
				$scope.searchString = null;
				$location.search('search', null).path('/');
			}

			else {

				// set the url search var
				$location.search('search', searchStr).path('/');

				// set the search bar variable
				$scope.searchString = searchStr;

				apiService.search(searchStr).then(function(result) {

					// no results
					if (result.data.code === 2) {
						$scope.numResults = 'No results';
					}

					// has results
					else if (result.data.code === 1) {
						$scope.numResults = result.data.data.length + ' results';
						$scope.searchResults = buildCompanyAndProductLinks(result.data.data);
					}

					else {
						$scope.numResults = 'error';
						console.log(result.data);
					}

				});
			}
		};





		var initializeSearchAtPageLoad = function() {

			var searchStr = $location.search().search;

			// if search string exists
			if (typeof searchStr !== 'undefined') {
				$scope.submitSearch(searchStr);
			}
		};

		initializeSearchAtPageLoad();


		var buildCompanyAndProductLinks = function(data) {

			for (var i=0; i<data.length;i++) {
				if (data[i].type=== 'product') {
					data[i].link = '#/products/'+data[i].id;
				}

				else if (data[i].type==='company') {
					data[i].link = '#/companies/'+data[i].id;
				}
			}

			return data;
		};





// ************************************************************************
// ************************************************************************
//            ALERT METHODS
// ************************************************************************
// ************************************************************************


//		$scope.alerts = alertsService.alerts;

//		$scope.closeAlert = function(index) {
//			alertsService.closeAlert(index);
//		};



	}]);
