'use strict';

angular.module('b2bwebApp')

	.controller('LoginCtrl', ['$scope', '$http', '$location', 'b2bGlobals', '$cookieStore', '$sanitize', 'localStorageService', 'sessionsService', 'alertsService', 'RoutingService', function($scope, $http, $location, b2bGlobals, $cookieStore, $sanitize, localStorageService, sessionsService, alertsService, RoutingService) {

		alertsService.registerAlertsMethods($scope);

		sessionsService.resetSession();

// ************************************************************************
// ************************************************************************
//            LOGIN METHODS
// ************************************************************************
// ************************************************************************


		$scope.attemptLogin = function() {

			var formEmail = $sanitize($scope.email);
			var formPassword = $sanitize($scope.password);

			if ($scope.showResetPasswordOptions === true) {
				var formNewPassword1 = $sanitize($scope.newpassword1);
				var formNewPassword2 = $sanitize($scope.newpassword2);

				// check both password are not empty
				if (formNewPassword1.trim().length === 0 || formNewPassword2.trim().length === 0) {
					$scope.alerts = alertsService.addAlert('Please enter new passwords', 'danger');
					return;
				}

				// check both password are not empty
				if (formNewPassword1 !== formNewPassword2) {
					$scope.alerts = alertsService.addAlert('New passwords don\'t match', 'danger');
					return;
				}
			}

			// error check inputs

			if (formEmail === '' || formEmail === null || typeof formEmail === 'undefined') {
				$scope.alerts = alertsService.addAlert('Please enter email address', 'danger');
				return;
			}


			var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var passwordRegEx = /^.{6,15}$/;

			if (!emailRegEx.test(formEmail)) {
				$scope.alerts = alertsService.addAlert('Please enter a valid email address', 'danger');
				return;
			}



			if (formPassword === '' || formPassword === null || typeof formPassword === 'undefined') {
				$scope.alerts = alertsService.addAlert('Please enter password', 'danger');
				return;
			}


			if (!passwordRegEx.test(formPassword)) {
				$scope.alerts = alertsService.addAlert('Login incorrect, please try again', 'danger');
				return;
			}


			var loginData = {
				'email' : formEmail,
				'pword' : formPassword
			};

			// login
			$http({
				'method' : 'POST',
				'url' : b2bGlobals.APIServer + '/login',
				'data' : loginData
			})
				.success(
				function(loginResult) {

					// user logged in with temp password
					if (loginResult.code === 555) {
						$scope.showResetPasswordOptions = true;
					}

					// login is successful
					else if (loginResult.code === 1) {

						// reset alerts
						alertsService.resetAlerts($scope);

						// build session object
						var sessObj = {
							'sessionid' : loginResult.data.sessionid,
							'email' : formEmail,
							'firstname' : loginResult.data.firstname,
							'lastname' : loginResult.data.lastname
						};

						// save session key in localStorage
						sessionsService.saveNewSession(sessObj);

						console.log(RoutingService.getIntendedRoute());

						if (RoutingService.indendedRoute !== '') {
							$location.path(RoutingService.getIntendedRoute());
						}

						else {
							$location.path('/main');
						}
					}

					// unsuccessful login
					else {
						$scope.alerts = alertsService.addAlert('Login incorrect, please try again', 'danger');
					}

				}
			);
		};




	}]);
