'use strict';

angular.module('b2bwebApp')

	.controller('AdminCtrl', ['$scope', 'sessionsService', '$location', 'localStorageService', function($scope, sessionsService, $location, localStorageService) {

		$scope.showLogoAtTop = true;

		sessionsService.setSessionObject($scope);


		sessionsService.isLocalSessionValid().then(function(result) {


			console.log('local session is valid');
			console.log(result);
			// local session is valid
			if (result === true) {
				sessionsService.isServerSessionValid().then(function(result2) {

					console.log(result2.data);

					// server session is valid
					if (result2.data.code === 1) {
//						$location.path('/admin');
						$scope.usersname = localStorageService.get('session').firstname;
					}
					// server session is invalid
					else {
						$location.path('/welcome');
					}
				});
			}
			// local session is invalid
			else {
				$location.path('/welcome');
			}
		});



//		$scope.logoutUser = function() {
//			sessionsService.deleteSession();
//			$location.path('/login');
//		};



	}]);