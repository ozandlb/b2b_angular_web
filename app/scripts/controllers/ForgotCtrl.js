'use strict';


angular.module('b2bwebApp')

  .controller('ForgotCtrl', ['$scope', '$sanitize', 'b2bGlobals', '$routeParams', 'alertsService', '$http', function($scope, $sanitize, b2bGlobals, $routeParams, alertsService, $http) {

 

		$scope.requestPasswordReset = function() {


      if ($scope.alerts.length > 0) {
        $scope.alerts = [];
      }

      var formEmail = $sanitize($scope.email);

      // error check inputs

      if (formEmail === '' || formEmail === null || typeof formEmail === 'undefined') {
        alertsService.addAlert('Please enter email address', 'danger');
        return;
      }


      var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      
      if (!emailRegEx.test(formEmail)) {
        alertsService.addAlert('Please enter a valid email address', 'danger');
        return;
      }


			var resetRequestData = {
			  'email' : formEmail
			};

	    // login
	    $http({
	      'method' : 'POST',
	      'url' : b2bGlobals.APIServer + '/forgot',
	      'data' : resetRequestData
	    })
	    .success(
	      function(resetRequestResult) {

	        // reset is successful
	        if (resetRequestResult.code === 1) {

						alertsService.addAlert('Please check your email for password reset instructions', 'success');
	          // apply as not using angular's $http
	          $scope.$apply();

	        }
	     
	        else if (resetRequestResult.code === 2) {
						alertsService.addAlert('Error, email does not exist', 'danger');
	          $scope.$apply();
	        }

	        else {
						alertsService.addAlert('Error, please try again', 'danger');

	          // apply as not using angular's $http
	          $scope.$apply();

	        }

	      }
	    );


		};



		$scope.loadPasswordResetHash = function() {

			// load by hiding inputs			
			$scope.showPasswordInputs = false;
			alertsService.addAlert('Loading...', 'success');

			// sanitize inputted resethash
			var sanResetHash = $sanitize($routeParams.resetHash);


			// check API if reset hash is still valid

			$http({
	      'method' : 'POST',
	      'url' : b2bGlobals.APIServer + '/forgot/' + sanResetHash
	    })
	    .success(
	      function(checkHashStatusResult) {

	        // hash is valid
	        if (checkHashStatusResult.code === 1) {

	          console.log('hash is still valid');
						// show new password inputs
						$scope.showPasswordInputs = true;
						$scope.alerts = [];

	          // apply as not using angular's $http
	          $scope.$apply();

	        }
	     
	        else if (checkHashStatusResult.code === 2) {
						alertsService.addAlert('Expired reset request', 'danger');
	          $scope.$apply();
	        }

	        else {
						alertsService.addAlert('Error, please try again', 'danger');

	          // apply as not using angular's $http
	          $scope.$apply();

	        }

	      }
	    );




		};



		$scope.attemptResetPassword = function() {

			// sanitize inputs
			var sanPassword1 = $sanitize($scope.password1);
			var sanPassword2 = $sanitize($scope.password2);
			var sanResetHash = $sanitize($routeParams.resetHash);

			// check password meets criteria
			var passwordRegEx = /^.{6,15}$/;
			if (!passwordRegEx.test(sanPassword1)) {
				alertsService.addAlert('Password must be between 6 and 15 characters', 'danger');
        return;
      }

			// check the passwords are the same
			if (sanPassword1 !== sanPassword2) {
				alertsService.addAlert('Passwords don\'t match', 'danger');
				return;
			}

			// send hash and new password

			var resetData = {
			  'resetCode' : sanResetHash,
			  'newPassword' : sanPassword1
			};

	    // login
	    $http({
	      'method' : 'POST',
	      'url' : b2bGlobals.APIServer + '/reset',
	      'data' : resetData
	    })
	    .success(
	      function(resetRequestResult) {

	        // reset is successful
	        if (resetRequestResult.code === 1) {

						alertsService.addAlert('Password changed successfully', 'success');
	          // apply as not using angular's $http
	          $scope.$apply();

	        }
	     
	        else if (resetRequestResult.code === 2) {
						alertsService.addAlert('Reset request not found', 'danger');
	          $scope.$apply();
	        }

	        else {
						alertsService.addAlert('Error', 'danger');

	          // apply as not using angular's $http
	          $scope.$apply();

	        }

	      }
	    );




		};



// ************************************************************************
// ************************************************************************
//            ALERT METHODS
// ************************************************************************
// ************************************************************************


    $scope.alerts = alertsService.alerts;

  }]);