'use strict';

angular.module('b2bwebApp')

	.controller('ProfileCtrl', ['$scope', '$location', 'b2bGlobals', '$cookieStore', '$sanitize', 'localStorageService', 'sessionsService', 'alertsService', 'UsersManager', function ($scope, $http, $location, b2bGlobals, $cookieStore, $sanitize, localStorageService, sessionsService, alertsService, UsersManager) {


		function getUserDetails(userid) {
			UsersManager.getUserInfo(userid).then(function(result) {

				console.log(result.data.code);

				if (result.data.code === 900) {
					$location.path('/welcome');
					return;
				}

				$scope.user = result.data.data.user;
				$scope.reviews = result.data.data.reviews;
			});
		}


		sessionsService.isLocalSessionValid().then(function(result) {
			if (result === true) {
				getUserDetails(localStorageService.get('session').id);
			}
			else {
				console.log('session is not valid');
				$location.path('/welcome');
			}
		});




		$scope.logoutUser = function() {
			sessionsService.deleteSession();
			$location.path('/login');
		};


	}]);



